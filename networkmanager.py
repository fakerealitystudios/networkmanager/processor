#!/usr/bin/python

import mysql.connector
from mysql.connector import errorcode
from difflib import SequenceMatcher
import math
import yaml
import json

config = {
	'version': 1,
	'api': {
		'endpoint': 'https://api.networkmanager.app',
		'statistics': True
	},
	'database': {
		'hostname': '',
		'port': 3306,
		'database': '',
		'username': '',
		'password': ''
	}
}
try:
	config = yaml.load(open('config.yml', 'r'))
	print(config)
except:
	stream = open('config.yml', 'w')
	yaml.dump(config, stream, default_flow_style=False)
	stream.close()

print(config)

db = None
try:
	db = mysql.connector.connect(
		user=config['database']['username'],
		password=config['database']['password'],
		host=config['database']['hostname'],
		database=config['database']['database'],
		port=config['database']['port']
	)
except mysql.connector.Error as err:
	if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
		print("Something is wrong with your user name or password")
	elif err.errno == errorcode.ER_BAD_DB_ERROR:
		print("Database does not exist")
	else:
		print(err)

if db is None:
	raise SystemExit

cursor = db.cursor()

query = ("SELECT badge, stat, value FROM badges")
cursor.execute(query)
badges = cursor.fetchall()

query = ("SELECT rank, realm, subgrouping, inherit FROM ranks")
cursor.execute(query)
rankrows = cursor.fetchall()

def getRankLevel(inherit):
	level = 0
	for (rank, realm, subgrouping, inheritt) in rankrows:
		if (inherit == rank):
			level += 1
			if inheritt != "":
				level += getRankLevel(inheritt)
			break
	return level

ranks = {}
ranksMap = {}
for (rank, realm, subgrouping, inherit) in rankrows:
	ranksMap[rank] = [rank, realm, subgrouping, inherit]
	if subgrouping not in ranks:
		ranks[subgrouping] = {}
	level = getRankLevel(inherit)
	if level not in ranks[subgrouping]:
		ranks[subgrouping][level] = []
	ranks[subgrouping][level].append([rank, realm, subgrouping, inherit])

def processMember(nmid,displayName,email):
	query = ("SELECT badge FROM memberBadges "
		"WHERE uid = %s")
	cursor.execute(query, (nmid,))

	mbadges = []
	for (badge,) in cursor.fetchall():
		mbadges.append(badge)
	print("\tBadges: {}".format(mbadges))

	query = ("SELECT rank, realm FROM memberRanks "
		"WHERE uid = %s")
	cursor.execute(query, (nmid,))

	mrank = "user"
	mranks = {}
	for (rank, realm) in cursor:
		mranks[rank] = realm
		if ranksMap[rank][2] == 0 and realm == "*": # Only administration ranks in the global realm
			if rank != "user" and getRankLevel(mrank) < getRankLevel(rank): # pick our highest rank always.
				mrank = rank

	query = ("SELECT COUNT(*), SUM(endTime - startTime) FROM memberSessions "
		"WHERE uid = %s AND `endTime` != -1")
	cursor.execute(query, (nmid,))

	tmp = cursor.fetchall()[0]
	playtime = 0
	sessions = 0
	if tmp[1] is not None:
		playtime = tmp[1]
		sessions = tmp[0]
	tmp = None
	
	logs = {}
	query = ("SELECT `event`, COUNT(*) FROM `logs` WHERE `type` = 'PLAYER' AND `id` IN (SELECT DISTINCT `lid` FROM `logDetails` "
		"WHERE `argid` = 0 AND `type` = 'NMID' AND `value` = %s) "
		"GROUP BY `event`")
	cursor.execute(query, (nmid,))

	for (event, count) in cursor:
		logs[event] = count
	print("\tLogs: {}".format(logs))
	
	# Alright, we have our data, now to perform the processing
	for (badge, stat, value) in badges:
		if badge in mbadges:
			continue
		awardBadge = False
		if stat == "playtime" and playtime >= value:
			awardBadge = True
		elif stat in logs and logs[stat] >= value:
			awardBadge = True
		
		if awardBadge:
			print("\t- Awarding badge {}".format(badge))
			query = ("INSERT INTO `memberBadges`(`uid`, `badge`) VALUES (%s, %s)")
			cursor.execute(query, (nmid, badge))

	# I know, I'm hardcoding member in... and these times and shit, at some point I'll fix this
#	if getRankLevel("member") > getRankLevel(mrank): # check if player is member or higher at some point...
#		if (playtime >= 2 * 60 * 60 and len(sessions) >= 5) or ("chat" in logs and logs["chat"] > 200): # See if the player has good playtime and sessions number
#			query = ("INSERT INTO `memberRanks`(`uid`, `rank`, `realm`) VALUES (%s, %s, %s)")
#			cursor.execute(query, (nmid, "member", "*"))
#			print("\t- Player is now a Member! Congrats!")
#		else:
#			print("\t- Player does not meet the member stats!")

	query = ("SELECT DISTINCT `ip` FROM `memberSessions` "
		"WHERE `uid` = %s AND `ip` IS NOT NULL")
	cursor.execute(query, (nmid,))
	ips = []
	for (ip,) in cursor.fetchall():
		ips.append(ip)
	
	# Now to figure out all the possible alts we have!
	# First, gather all of our current guesses so we don't create duplicates
	query = ("SELECT `otheruid`, `triggers` FROM `memberAlts` "
		"WHERE `uid` = %s")
	cursor.execute(query, (nmid,))
	alts = {}
	for (othernmid,triggers,) in cursor.fetchall():
		alts[othernmid] = json.loads(triggers)
	query = ("SELECT `uid`, `triggers` FROM `memberAlts` "
		"WHERE `otheruid` = %s")
	cursor.execute(query, (nmid,))
	for (othernmid,triggers,) in cursor.fetchall():
		alts[othernmid] = json.loads(triggers)
	
	newalts = {}
	# Second, gather all nmids that we believe may be a duplicate of this one
	for (other_nmid,other_displayName,other_email,other_validated) in members:
		if nmid == other_nmid:
			continue
		
		if (not other_nmid in alts or not "duplicate_email" in alts[other_nmid]) and other_validated and not email is None and email == other_email:
			if not other_nmid in newalts:
				newalts[other_nmid] = []
			newalts[other_nmid].append("duplicate_email")

		if (not other_nmid in alts or not "duplicate_name" in alts[other_nmid]) and displayName == other_displayName:
			if not other_nmid in newalts:
				newalts[other_nmid] = []
			newalts[other_nmid].append("duplicate_name")

		if (not other_nmid in alts or not "similar_name" in alts[other_nmid]) and SequenceMatcher(None, displayName, other_displayName).ratio() >= 0.7:
			if not other_nmid in newalts:
				newalts[other_nmid] = []
			newalts[other_nmid].append("similar_name")

	ipalts = []
	for k,v in alts.items():
		if "duplicate_ip" in v:
			ipalts.append(k)
	
	if len(ips) > 0:
		query = ("SELECT DISTINCT `uid` FROM `memberSessions` "
			"WHERE `uid` != %s AND `ip` IN (")
		query = query + ','.join(['%s'] * len(ips))
		query = query+')'
		if len(ipalts) > 0:
			query = query+' AND `uid` NOT IN ('
			query = query + ','.join(['%s'] * len(ipalts))
			query = query+')'
		qrylist = [nmid]
		qrylist.extend(ips)
		if len(ipalts) > 0:
			qrylist.extend(ipalts)
		cursor.execute(query, tuple(qrylist))
		for (other_nmid,) in cursor.fetchall():
			if not other_nmid in newalts:
				newalts[other_nmid] = []
			newalts[other_nmid].append("duplicate_ip")

	# Third, insert two records, one for us AND one for them
	if len(newalts) > 0:
		query = "INSERT INTO `memberAlts`(`uid`, `otheruid`, `triggers`, `likelihood`) VALUES "
		qrylist = []
		for k,v in newalts.items():
			qrylist.append(nmid)
			qrylist.append(k)
			qrylist.append(json.dumps(v))
			qrylist.append(0.00)
			query += "(%s,%s,%s,%s),"
		query = query[:-1] # remove final comma
		cursor.execute(query, tuple(qrylist))
	print("Found {} new possible alts".format(len(newalts)))
	
	db.commit() # Commit all changes we've made to the current user

# Process members
query = ("SELECT COUNT(id) FROM `members` WHERE `newid` IS NULL")
cursor.execute(query)
membercount = cursor.fetchall()[0][0]

if (True):
	query = ("SELECT id, displayName, email, validated FROM members WHERE `newid` IS NULL")
	cursor.execute(query)
	members = cursor.fetchall()
	for (nmid,displayName,email,validated) in members:
		print("--------------------")
		print("Processing user {} out of {}: {}".format(nmid, membercount, displayName))
		print("--------------------")
		if validated:
			processMember(nmid, displayName, email)
		else:
			processMember(nmid, displayName, None)

# Process Member Sessions
#query = ("SELECT `id`,`uid`,`sid`,`ssid`,`startTime`,`endTime` FROM `memberSessions` WHERE `invalidated` = 1")
#cursor.execute(query)

# Process Server Sessions


db.close()