from pip._internal import main as pip

def install(package):
    pip(['install', package])

install('mysql-connector-python')
install('pyyaml')