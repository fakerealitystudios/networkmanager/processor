#!/usr/bin/python

import time
from importlib import reload
import networkmanager

wait = 3600

while True:
    print("Waiting " + str(wait) + " seconds")
    time.sleep(wait) # wait seconds between each process
    reload(networkmanager)
